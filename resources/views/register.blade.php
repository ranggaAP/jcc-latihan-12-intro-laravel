<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="{{ URL::to('/welcome') }}" method="POST">
        @csrf
        <label>First Name:</label>
        <br><br>
        <input type="text" id="first_name" name="first_name" required>
        <br><br>
        <label>Last Name:</label>
        <br><br>
        <input type="text" id="last_name" name="last_name" required>
        <br><br>
        <label>Gender:</label>
        <br><br>
        <input type="radio" id="male" name="fav_language" value="male" required>
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="fav_language" value="female">
        <label for="female">Female</label><br>
        <br><br>
        <label>Nationality</label>
        <br><br>
        <select name="nationality" id="nationality" required>
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="sunda_empire">Sunda Empire</option>
        </select>
        <br><br>
        <label>Language Spoken</label>
        <br><br>
        <input type="checkbox" id="bahasa_indonesia" name="bahasa_indonesia" value="bahasa_indonesia">
        <label for="bahasa_indonesia"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="english">
        <label for="english"> English</label><br>
        <input type="checkbox" id="other" name="other" value="other">
        <label for="other"> Other</label><br>
        <br><br>
        <label>Bio</label>
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10" required></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>