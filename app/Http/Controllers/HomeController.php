<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function welcome(Request $request)
    {
        $first_name = $request->first_name;
        $last_name  = $request->last_name;

        $name = $first_name.' '.$last_name;

        $data = [
            'name'  => $name
        ];

        return view('welcome', $data);
    }
}
